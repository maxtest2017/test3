module.exports = function(bot) {
    bot.dialog('/welcome', [
        function (session, args, next) {
            const lastVisit = session.userData.lastVisit;

            session.send(['Hello!', 'Hi there!', 'Hi!']);

            if (!lastVisit) {
                session.send('Our store carries bikes, parts, accessories, and sport clothing articles');
                session.userData = Object.assign({}, session.userData, {
                    lastVisit: new Date()
                });
                session.save();
            } else {
                session.send('Glad you\'re back!');
            }

            session.endDialog('How can I help you?');
        }
    ]);
};


/*
bot.dialog('/ensureProfile', [
    // first function to initilze the diaglog
    function (session, args, next) {
       if (!session.userData.token) {
	    builder.Prompts.text(session, 'Hi, my name is Miles. Please enter user code from your House app. You can also use Demo code 9QHXFD2');
               } 
        else { 
        session.send('I found you used the code [ %s ] last time',session.userData.token );        
        builder.Prompts.choice(session, "Do you want to use it again?","yes|no", {listStyle: builder.ListStyle.button});
             }
    },
    // to confirm if the uesr wants to use the saved code or not
	function (session, results,next) {
       if(results.response.entity == "yes") 
        {
            results.repsonse = session.userData.token;
        }
        else if (results.response.entity == "no")
        {
        session.userData.token = null;
        session.endConversation('You can restart the conversation and enter the code again.');               
        } else {
        session.userData.token = results.response;    
        }
    // read Room Information		
	    if(session.userData.token)
        {
		House.getHouseProfile(session.userData.token)			
		    .then( function (res) { 
		    session.userData.user = res; 
		    session.userData.cognitoId = res.cognito_id;
    	    session.userData.NumberId = res.current_Number;
            if (res.current_Number)        {
            session.send('I found your Number number: %s .',session.userData.NumberId );                                        }
            else{
            session.userData.token = null;    
            session.send('Cant find Room Number.');               
            session.endConversation('You can restart the conversation and enter the code again.');     
                 }
			})
	        .catch( function (err) {
            session.userData.token = null;    	
            session.send('Something wrong with the code.');               
            session.endConversation('You can restart the conversation and enter the code again.');     
                               }) 	
                }
          else{}	
    //end of function
        },
    function (session, results,next) {
    // session.begindialog('/HouseQnA', basicQnAMakerDialog);     
    }                                  
    //end of dialog                            
	   	]);

*/