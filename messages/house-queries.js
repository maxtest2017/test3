var Promise = require('bluebird');

function HouseQueryFragment( directive, data ) {
	this.directive = directive;
	this.data = data;
}

var kQueryables = {
	house: {
			endpoint: 'https://api.Houseservices.com/v1-bot/Room/{Number}',
			isCollection: false,
			properties: [
			'model', 'model_year_id', 'name', 'num_doors','users',
			'Room_id', 'Room_size', 'Room_style', 'Room_type',
			'Number', 'year'
			]
	},
	user: {
			endpoint: 'https://api.Houseservices.com/v1-bot/user',
			isCollection: false,
			properties: [
				'cognito_id', 'email', 'full_name', 'current_Number'
			]
	},
	backlog: {
			endpoint: 'https://api.Houseservices.com/v1-bot/backlog',
			isCollection: true,
			properties: [
				'action_id', 'backlog_id', 'appointment_type', 'scheduled_time',
				'scheduled_months', 'status', 'Room_id'
			]
	}
}
var kProviderMap = {
	house: HouseApi.getFirstRoomInformationForUser,
	user: HouseApi.getUserProfile,
	backlog: HouseApi.getFirstBacklogInformationForUser
};

