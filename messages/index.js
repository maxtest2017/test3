/*-----------------------------------------------------------------------------
This template demonstrates how to use an IntentDialog with a LuisRecognizer to add 
natural language support to a bot. 
For a complete walkthrough of creating this type of bot see the article at
http://docs.botframework.com/builder/node/guides/understanding-natural-language/
-----------------------------------------------------------------------------*/
"use strict";
var builder = require("botbuilder");
var botbuilder_azure = require("botbuilder-azure");

var useEmulator = (process.env.NODE_ENV == 'development');
useEmulator = 1;

var connector = useEmulator ? new builder.ChatConnector() : new botbuilder_azure.BotServiceConnector({
    appId: process.env['MicrosoftAppId'],
    appPassword: process.env['MicrosoftAppPassword'],
    stateEndpoint: process.env['BotStateEndpoint'],
    openIdMetadata: process.env['BotOpenIdMetadata']
});


const greeting = require('./app/recognizer/greeting');
const commands = require('./app/recognizer/commands');
const smiles = require('./app/recognizer/smiles');

const dialog = {
    welcome: require('./app/dialogs/welcome'),
    explore: require('./app/dialogs/explore'),
};

var bot = new builder.UniversalBot(connector);

// Make sure you add code to validate these fields
var luisAppId = process.env.LuisAppId;
var luisAPIKey = process.env.LuisAPIKey;
var luisAPIHostName = process.env.LuisAPIHostName || 'api.projectoxford.ai';

// const LuisModelUrl = 'https://' + luisAPIHostName + '/luis/v1/application?id=' + luisAppId + '&subscription-key=' + luisAPIKey;
const LuisModelUrl='https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/1ab3d2cd-1d79-4418-8e4b-876728ee9f89?subscription-key=28a5e4a8d7934249bfd5531ec8f3ea56&verbose=true&spellCheck=true&q=';

const greeting = require('./app/recognizer/greeting');
const commands = require('./app/recognizer/commands');
const smiles = require('./app/recognizer/smiles');

const dialog = {
    welcome: require('./app/dialogs/welcome'),
    explore: require('./app/dialogs/explore'),
};

if (useEmulator) {
    var restify = require('restify');
    var server = restify.createServer();
    server.listen(3978, function() {
        console.log('test bot endpont at http://localhost:3978/api/messages');
    });
    server.post('/api/messages', connector.listen());    
} else {
    module.exports = { default: connector.listen() }
}

// Main dialog with LUIS
var intents = new builder.IntentDialog({
    recognizers: [
        commands,
        greeting,
        smiles,
        // new builder.LuisRecognizer(process.env.LUIS_ENDPOINT)
        new builder.LuisRecognizer(LuisModelUrl)
    ],
    intentThreshold: 0.2,
    recognizeOrder: builder.RecognizeOrder.series
});

var recognizerQnA = new builder_cognitiveservices.QnAMakerRecognizer({
                // knowledgeBaseId: process.env.QnAKnowledgebaseId, 
                 knowledgeBaseId: "ccc47a1e-2624-4a49-9c09-b11251cbe590",
                 // subscriptionKey: process.env.QnASubscriptionKey});
                 subscriptionKey: "14c03ba8be204db7b61df5b6582b6ae4"});

var basicQnAMakerDialog = new builder_cognitiveservices.QnAMakerDialog({
    recognizers: [recognizerQnA],
                defaultMessage: 'Sorry I cant understand the question. Please contact catfit.',
                qnaThreshold: 0.3}
);

// var recognizer = new builder.LuisRecognizer(LuisModelUrl);
// var intents = new builder.IntentDialog({ recognizers: [recognizer] });
intents.matches('Greeting', '/welcome');
intents.matches('Schedule', '/schedule');
intents.matches('Reset', '/reset');
intents.matches('Smile', '/smileBack');

intents.onDefault('/HouseQnA');

bot.dialog('/', intents);
dialog.welcome(bot);
dialog.schedule(bot);


bot.dialog('/HouseQnA', [
    function (session, args, next) {
        // if No Intents found then switch to QnA
        // session.beginDialog('/HouseQnA', session.userData.profile);
   }
]);
//  bot.dialog('/HouseQnA', basicQnAMakerDialog).triggerAction({ matches: 'qna' });
 

bot.dialog('/reset', [
    function (session, args, next) {
        session.endConversation(['See you later!', 'bye!']);
    }
]);

bot.dialog('/smileBack', [
    function (session, args, next) {
        const smile = builder.EntityRecognizer.findEntity(args.entities, 'Smile');
        const type = builder.EntityRecognizer.findEntity(args.entities, 'Type');

        if (!smile || !smile.entity) {
            session.endDialog('<ss type="smile">:)</ss>');
        } else {
            session.endDialog(`<ss type="${type.entity}">${smile.entity}</ss>`);
        }
    }
]);
